/* 
 * File:   MBSet.cu
 * Author: Ning Wang
 * Created on Nov 23, 2012
 * 
 * Purpose:  This program displays Mandelbrot set using the GPU via CUDA and
 * OpenGL immediate mode.
 * 
 */

#include <iostream>
#include <stack>
#include <cuda_runtime_api.h>
#include <stdio.h>
#include <cmath>
#include "Complex.cu"

#include <GL/freeglut.h>

// Size of window in pixels, both width and height
#define WINDOW_DIM 512
#define N WINDOW_DIM * WINDOW_DIM
#define THREADS_PER_BLOCK 32
#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)
#define PROJECT(a, b, c) ((b - c) / WINDOW_DIM * a + c)

using namespace std;

// Initial screen coordinates, both host and device.
const int maxIt = 2000; // Msximum Iterations
int last_mx = 0, last_my = 0, cur_mx = 0, cur_my = 0, _x = 0, _y = 0;
bool mouse_down = false;
int* iter_count;

typedef pair<double, double> mypair;
typedef pair< mypair, mypair > record;
typedef stack<record> history;
history hist;

// Define the RGB Class
class RGB
{
public:
    RGB() : r(0), g(0), b(0) {}
    RGB(double r0, double g0, double b0) : r(r0), g(g0), b(b0) {}
public:
    double r;
    double g;
    double b;
};

RGB* colors = 0; // Array of color values

void InitializeColors()
{
  colors = new RGB[maxIt + 1];
  for (int i = 0; i < maxIt; ++i)
  {
      if (i < 5)
          colors[i] = RGB(1, 1, 1);
      else
          colors[i] = RGB(drand48(), drand48(), drand48());
  }
  colors[maxIt] = RGB(); // black
}

static mypair minC(-2.0, -1.2);
static mypair maxC(1.0, 1.8);

__global__ void compute(int* d_iter_count, mypair* d_h, mypair* d_l)
{
    int index = threadIdx.x + blockIdx.x * blockDim.x;
    double step_r = (d_h->first - d_l->first) / WINDOW_DIM;
    double step_i = (d_h->second - d_l->second) / WINDOW_DIM;
    Complex c(d_l->first + index % WINDOW_DIM * step_r, d_l->second + index / WINDOW_DIM * step_i);
    Complex z(c);

    for (int k = 1; k <= maxIt; k++) {
        z = z * z + c;
        d_iter_count[index] = k;
        if (sqrt(z.magnitude2()) > 2.0)
            break;
    }
}

void displayHelper(mypair* l, mypair* h, bool re_calc)
{   
    if (re_calc) {
        int* d_iter_count;
        mypair* d_l;
        mypair* d_h;
        cudaMalloc((void**) &d_l, sizeof(mypair));
        cudaMalloc((void**) &d_h, sizeof(mypair));
        cudaMalloc((void**) &d_iter_count, N * sizeof(int));
        cudaMemcpy(d_l, l, sizeof(mypair), cudaMemcpyHostToDevice);
        cudaMemcpy(d_h, h, sizeof(mypair), cudaMemcpyHostToDevice);
        compute<<<N / THREADS_PER_BLOCK,THREADS_PER_BLOCK>>>(d_iter_count, d_h, d_l);
        cudaMemcpy(iter_count, d_iter_count, N * sizeof(int), cudaMemcpyDeviceToHost);
        cudaFree(d_l);
        cudaFree(d_h);
        cudaFree(d_iter_count);
    }
    
    GLfloat* color = new GLfloat[3];
    glBegin(GL_POINTS);
    for (int i = 0; i < WINDOW_DIM; i++) {
        for (int j = 0; j < WINDOW_DIM; j++) {
            int iters = iter_count[i + j * WINDOW_DIM];
            color[0] = colors[iters].r;
            color[1] = colors[iters].g;
            color[2] = colors[iters].b;
            glColor3fv(color);
            glVertex2f(i, j);
        }
    }
    glEnd();
}

void display()
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, WINDOW_DIM, WINDOW_DIM, 0, 0, 1);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glClearColor(.0, .0, .0, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    displayHelper(&minC, &maxC, true);
    glFlush();
    glutSwapBuffers(); // If double buffering
}

void mouse(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        mouse_down = true;
        last_mx = cur_mx = x;
        last_my = cur_my = y;
    } else {
        if (mouse_down) {
            // redraw the mandelbrot set
            int diff_x = cur_mx - last_mx;
            int diff_y = cur_my - last_my;

            if (diff_x > diff_y) {
                _y = last_my + diff_y;
                _x = cur_mx;
            } else {
                _y = cur_my;
                _x = last_mx + diff_x;
            }

            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(0, WINDOW_DIM, WINDOW_DIM, 0, 0, 1);
            glDisable(GL_DEPTH_TEST);
            glMatrixMode(GL_MODELVIEW);
            glClearColor(.0, .0, .0, 0);
            glClear(GL_COLOR_BUFFER_BIT);
            displayHelper(&minC, &maxC, false);
            glColor3f(1.0, .0, .0);
            glBegin(GL_LINE_LOOP);
            glVertex2i(last_mx, last_my);
            glVertex2i(_x, last_my);
            glVertex2i(_x, _y);
            glVertex2i(last_mx, _y);
            glVertex2i(last_mx, last_my);
            glEnd();
            glFlush();
            glutSwapBuffers(); // If double buffering

            record r(minC, maxC);
            hist.push(r);
            mypair old_minC(minC.first, minC.second);
            mypair old_maxC(maxC.first, maxC.second);
            minC.first = PROJECT(MIN(last_mx, _x), old_maxC.first, old_minC.first);
            minC.second = PROJECT(MIN(last_my, _y), old_maxC.second, old_minC.second);
            maxC.first = PROJECT(MAX(last_mx, _x), old_maxC.first, old_minC.first);
            maxC.second = PROJECT(MAX(last_my, _y), old_maxC.second, old_minC.second);

            displayHelper(&minC, &maxC, true); // redraw mandelbrot set
            glFlush();
            glutSwapBuffers(); // If double buffering
        }
        mouse_down = false;
    }
}

void motion(int x, int y) {
    if (mouse_down) {
        cur_mx = x;
        cur_my = y;
    }
}

void keyboard(unsigned char key, int x, int y)
{
    if (key == 'b' && hist.size() > 0) {
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, WINDOW_DIM, WINDOW_DIM, 0, 0, 1);
        glDisable(GL_DEPTH_TEST);
        glMatrixMode(GL_MODELVIEW);
        glClearColor(.0, .0, .0, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        record r = hist.top();
        hist.pop();
        minC = r.first;
        maxC = r.second;
        displayHelper(&minC, &maxC, true);
        glEnd();
        glFlush();
        glutSwapBuffers(); // If double buffering
    }
}

int main(int argc, char** argv)
{
  // Initialize OPENGL here
  // Set up necessary host and device buffers
  // set up the opengl callbacks for display, mouse and keyboard
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(100,100);
    glutInitWindowSize(WINDOW_DIM, WINDOW_DIM);
    glutCreateWindow("Mandelbrot Set");
    glEnable(GL_DEPTH_TEST);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);

    iter_count = (int*) malloc(N * sizeof(int));

    // Calculate the interation counts
    InitializeColors();
    glutMainLoop(); // THis will callback the display, keyboard and mouse
    return 0;
}
